package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "t-unbind-to-project";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Task unbind to project.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }
}
