package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.command.AbstractCommand;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name:Alieva Djamilya");
        System.out.println("E-mail:jzama88@gmail.com");

    }
}
