package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "t-show-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show Task by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);

    }
}
