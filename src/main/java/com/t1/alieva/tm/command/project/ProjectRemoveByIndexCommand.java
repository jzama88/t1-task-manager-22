package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove Project by Index.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractEntityNotFoundException, AbstractUserException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }
}
