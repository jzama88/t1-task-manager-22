package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show Project by Index.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractUserException {
        System.out.println(("[SHOW PROJECT BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        final Project project = getProjectService().findOneByIndex(userId, index);
        showProject(project);
    }
}
