package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractUserException {
        System.out.println("[CLEAR PROJECT]");
        final String userId = getUserId();
        getProjectService().removeAll(userId);
    }
}
