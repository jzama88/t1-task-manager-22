package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "View profile of current user.";

    public static final String NAME = "user-view-profile";

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN:" + user.getLogin());
        System.out.println("FIRST NAME:" + user.getFirstName());
        System.out.println("MIDDLE NAME:" + user.getMiddleName());
        System.out.println("LAST NAME:" + user.getLastName());
        System.out.println("E-MAIL:" + user.getEmail());
        System.out.println("ROLE:" + user.getRole().getDisplayName());
        showUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
