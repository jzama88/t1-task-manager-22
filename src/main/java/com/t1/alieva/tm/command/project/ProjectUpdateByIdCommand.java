package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Update Project by ID.";
    }

    @Override
    public void execute() throws AbstractEntityNotFoundException, AbstractFieldException, AbstractUserException {
        System.out.println(("[UPDATE PROJECT BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().updateById(userId, id, name, description);
    }
}
