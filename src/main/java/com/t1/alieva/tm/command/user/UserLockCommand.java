package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return "user lock";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public Role[] getRoles() {
        return new Role[]
                {
                        Role.ADMIN
                };
    }
}
