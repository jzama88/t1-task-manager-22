package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getArgument() {
        return "Create new task.";
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().create(userId, name, description);
    }
}
