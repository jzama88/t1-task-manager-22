package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException {
        System.out.println("[CLEAR TASK]");
        final String userId = getUserId();
        getTaskService().removeAll(userId);
    }
}
