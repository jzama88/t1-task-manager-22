package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.model.ICommand;
import com.t1.alieva.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public String getArgument() {
        return "arg";
    }

    @Override
    public String getDescription() {
        return "Show application arguments.";
    }

    @Override
    public void execute() {
        System.out.println("ARGUMENTS");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
