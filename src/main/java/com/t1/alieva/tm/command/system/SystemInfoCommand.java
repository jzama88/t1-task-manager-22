package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.util.FormatUtil;

public final class SystemInfoCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "info ";
    }

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show System Info";
    }

    @Override
    public void execute() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory (bytes): " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory (bytes): " + usageMemoryFormat);
    }
}
