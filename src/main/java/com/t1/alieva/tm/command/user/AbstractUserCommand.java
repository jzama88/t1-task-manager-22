package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.api.service.ITaskService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public void showUser(final User user) throws AbstractEntityNotFoundException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    public String getArgument() {
        return null;
    }


}
