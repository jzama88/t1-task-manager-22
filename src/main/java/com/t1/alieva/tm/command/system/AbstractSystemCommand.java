package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
