package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "p-start-by-id";
    }

    @Override
    public String getDescription() {
        return "Start Project by ID.";
    }

    @Override
    public void execute() throws AbstractEntityNotFoundException, AbstractFieldException, AbstractUserException {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }
}
