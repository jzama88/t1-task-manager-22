package com.t1.alieva.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show application version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.20.0");
    }
}
