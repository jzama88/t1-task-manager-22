package com.t1.alieva.tm.command.system;

import com.t1.alieva.tm.api.service.ICommandService;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Close Application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
