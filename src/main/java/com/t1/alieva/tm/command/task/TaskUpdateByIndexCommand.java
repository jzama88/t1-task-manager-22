package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "t-update-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update Task by Index.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }
}
