package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.enumerated.TaskSort;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "t-list-show";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Display Task list.";
    }

    @Override
    public void execute() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {
        System.out.println("[TASK LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
