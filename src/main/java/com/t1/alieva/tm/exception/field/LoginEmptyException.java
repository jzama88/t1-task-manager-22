package com.t1.alieva.tm.exception.field;

import com.t1.alieva.tm.command.user.AbstractUserCommand;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import com.t1.alieva.tm.util.TerminalUtil;

import javax.naming.AuthenticationException;

public final class LoginEmptyException extends AbstractFieldException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

    public static final class UserLoginCommand extends AbstractUserCommand {

        public static final String DESCRIPTION = "User login.";

        public static final String NAME = "login";

        @Override
        public void execute() throws
                AbstractUserException,
                AbstractFieldException,
                AbstractEntityNotFoundException,
                AuthenticationException,
                AuthenticException {
            System.out.println("[USER LOGIN]");
            System.out.println("ENTER LOGIN:");
            final String login = TerminalUtil.nextLine();
            System.out.println("ENTER PASSWORD:");
            final String password = TerminalUtil.nextLine();
            getAuthService().login(login, password);
        }

        @Override
        public String getDescription() {
            return DESCRIPTION;
        }

        @Override
        public String getName() {
            return NAME;
        }

        @Override
        public Role[] getRoles() {
            return null;
        }
    }
}
