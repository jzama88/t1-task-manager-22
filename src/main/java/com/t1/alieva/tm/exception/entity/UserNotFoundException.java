package com.t1.alieva.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }
}
