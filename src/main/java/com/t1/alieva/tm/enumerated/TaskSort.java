package com.t1.alieva.tm.enumerated;

import com.t1.alieva.tm.comparator.CreatedComparator;
import com.t1.alieva.tm.comparator.NameComparator;
import com.t1.alieva.tm.comparator.StatusComparator;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    private final String displayName;

    private final Comparator<Task> comparator;

    TaskSort(final String displayName, final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
