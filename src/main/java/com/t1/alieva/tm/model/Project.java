package com.t1.alieva.tm.model;

import com.t1.alieva.tm.api.model.IWBS;
import com.t1.alieva.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project extends AbstractUserOwnedModel implements IWBS {

    private Date created = new Date();
    private String description = "";
    private String name = "";
    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Project(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name, Status status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
