package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.api.service.*;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.command.project.*;
import com.t1.alieva.tm.command.system.*;
import com.t1.alieva.tm.command.task.*;
import com.t1.alieva.tm.command.user.*;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.LoginEmptyException;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.repository.ProjectRepository;
import com.t1.alieva.tm.repository.TaskRepository;
import com.t1.alieva.tm.repository.UserRepository;
import com.t1.alieva.tm.service.*;
import com.t1.alieva.tm.util.TerminalUtil;

import javax.naming.AuthenticationException;


public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    private final IAuthService authService = new AuthService(userService);

    {

        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());

        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());

        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());

    }


    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void processCommand(final String command) throws
            AbstractFieldException,
            AbstractEntityNotFoundException,
            CommandNotSupportedException,
            AbstractUserException,
            AuthenticException, AuthenticationException {
        final AbstractCommand abstractcommand = commandService.getCommandByName(command);
        if (abstractcommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractcommand.getRoles());
        abstractcommand.execute();

    }

    private void initLogger() {
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processArgument(final String argument) throws
            ArgumentNotSupportedException,
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException,
            AuthenticException, AuthenticationException {
        final AbstractCommand abstractcommand = commandService.getCommandByArgument(argument);
        if (abstractcommand == null) throw new ArgumentNotSupportedException(argument);
        abstractcommand.execute();
    }


    private void exit() {
        System.exit(0);
    }

    public void run(final String[] args) throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            ArgumentNotSupportedException,
            AbstractUserException, AuthenticationException, AuthenticException {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("FAIL");
            }
        }
    }

    private void initDemoData() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {

        final User userTest = userService.create("test", "test");
        final User userCustom = userService.create("user", "user", "user@user.ru");
        final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "PROJECT_TEST1", "Project 1 for TestUser");
        projectService.create(userTest.getId(), "PROJECT_TEST2", "Project 2 for TestUser");
        projectService.create(userCustom.getId(), "PROJECT_CUSTOM1", "Project for CustomUser");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 1 for Admin");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 2 for Admin");

        taskService.create(userTest.getId(), "TASK_TEST1", "test task 1");
        taskService.create(userTest.getId(), "TASK_TEST2", "test task 2");
        taskService.create(userCustom.getId(), "TASK_CUSTOM1", "test task 1");
        taskService.create(userCustom.getId(), "TASK_CUSTOM2", "test task 2");
        taskService.create(userAdmin.getId(), "TASK_ADMIN1", "test task 1");
        taskService.create(userAdmin.getId(), "TASK_ADMIN2", "test task 2");

    }

    private boolean processArguments(final String[] args) throws
            ArgumentNotSupportedException,
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException, AuthenticationException, AuthenticException {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

}
