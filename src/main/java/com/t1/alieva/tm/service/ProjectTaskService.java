package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ProjectNotFoundException;
import com.t1.alieva.tm.exception.entity.TaskNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.ProjectIdEmptyException;
import com.t1.alieva.tm.exception.field.TaskIdEmptyException;
import com.t1.alieva.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(String userId, final String projectId, final String taskId) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(String userId, final String projectId) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeOneById(userId, task.getId());
        projectRepository.removeOneById(projectId);
    }

    @Override
    public void unbindTaskFromProject(String userId, final String projectId, final String taskId) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

}
