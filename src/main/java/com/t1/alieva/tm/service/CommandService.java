package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.service.ICommandService;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    public AbstractCommand getCommandByName(final String name) throws CommandNotSupportedException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException();
        return command;

    }

    public AbstractCommand getCommandByArgument(final String argument) throws ArgumentNotSupportedException {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException();
        return command;

    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
