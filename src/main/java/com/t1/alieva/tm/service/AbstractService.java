package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ModelNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IRepository<M> {
    protected final R repository;

    public AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public M add(M model) throws AbstractEntityNotFoundException {
        if (model == null) throw new ModelNotFoundException();
        return repository.add(model);
    }


    @Override
    public List<M> findAll(Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    public M findOneById(String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);

    }

    @Override
    public M findOneByIndex(Integer index) throws AbstractFieldException {
        if (index == null || index < 0 || index >= repository.getSize()) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);

    }

    @Override
    public M removeOne(M model) throws AbstractEntityNotFoundException, AbstractFieldException {
        if (model == null) throw new ModelNotFoundException();
        return repository.removeOne(model);
    }

    @Override
    public M removeOneById(String id) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeOneById(id);
    }

    @Override
    public M removeOneByIndex(Integer index) throws AbstractFieldException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize()) throw new IndexIncorrectException();
        return repository.removeOneByIndex(index);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public void removeAll(Collection<M> models) {
        repository.removeAll(models);
    }

    public void removeAll() {
        repository.removeAll();
    }


}
