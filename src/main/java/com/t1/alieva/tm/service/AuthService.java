package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.service.IAuthService;
import com.t1.alieva.tm.api.service.IUserService;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.LoginEmptyException;
import com.t1.alieva.tm.exception.field.PasswordEmptyException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.exception.user.PermissionException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;


import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        return userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) throws
            AbstractFieldException,
            AbstractEntityNotFoundException,
            AbstractUserException, AuthenticException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticException();
        final Boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) throw new AuthenticException();
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public Boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() throws AbstractUserException {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() throws
            AbstractUserException,
            AbstractFieldException {
        if (!isAuth()) throw new AccessDeniedException();
        final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(Role[] roles) throws
            AbstractUserException,
            AbstractFieldException {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }
}
