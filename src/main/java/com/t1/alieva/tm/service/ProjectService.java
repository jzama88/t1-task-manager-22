package com.t1.alieva.tm.service;

import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.service.IProjectService;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.ProjectNotFoundException;
import com.t1.alieva.tm.exception.field.*;
import com.t1.alieva.tm.model.Project;

import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {

        super(projectRepository);
    }

    @Override
    public Project create(final String userId, final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) throws AbstractFieldException, AbstractEntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }
}
