package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    AbstractCommand getCommandByArgument(String argument) throws ArgumentNotSupportedException;

    AbstractCommand getCommandByName(String name) throws CommandNotSupportedException;

    Collection<AbstractCommand> getTerminalCommands();
}
