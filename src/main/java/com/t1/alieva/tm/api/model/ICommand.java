package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AuthenticException;

import javax.naming.AuthenticationException;


public interface ICommand {

    String toString();

    String getName();

    String getArgument();

    String getDescription();

    void execute() throws AbstractEntityNotFoundException, AbstractFieldException, AbstractUserException, AuthenticationException, AuthenticException;
}
