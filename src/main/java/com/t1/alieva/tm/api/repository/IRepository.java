package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.field.IdEmptyException;
import com.t1.alieva.tm.exception.field.IndexIncorrectException;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {
    M add(M model) throws AbstractEntityNotFoundException;

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll();

    M findOneById(String id) throws IdEmptyException, AbstractFieldException;

    M findOneByIndex(Integer index) throws IndexIncorrectException, AbstractFieldException;

    M removeOne(M model) throws AbstractEntityNotFoundException, AbstractFieldException;

    M removeOneById(String id) throws AbstractFieldException, AbstractEntityNotFoundException;

    M removeOneByIndex(Integer index) throws IndexIncorrectException, AbstractFieldException;

    int getSize();

    boolean existsById(String id);

    void removeAll(Collection<M> models);

    void removeAll();
}
