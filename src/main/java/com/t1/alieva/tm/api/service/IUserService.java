package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.api.repository.IRepository;

public interface IUserService extends IRepository<User> {

    User create(String login, String password) throws AbstractFieldException, AbstractUserException, AbstractEntityNotFoundException;

    User create(String login, String password, String email) throws AbstractFieldException, AbstractUserException, AbstractEntityNotFoundException;

    User create(String login, String password, Role role) throws AbstractFieldException, AbstractUserException, AbstractEntityNotFoundException;

    User add(User user) throws AbstractEntityNotFoundException;

    User findByLogin(String login) throws AbstractFieldException, UserNotFoundException;

    User findByEmail(String email) throws AbstractFieldException;

    User removeByLogin(String login) throws AbstractFieldException, AbstractEntityNotFoundException;

    User removeByEmail(String email) throws AbstractEntityNotFoundException, AbstractFieldException;

    User setPassword(String id, String password) throws AbstractFieldException, AbstractEntityNotFoundException;

    User updateUser(String id, String firstName, String lastName, String middleName) throws AbstractFieldException, AbstractEntityNotFoundException;

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    void lockUserByLogin(String login) throws AbstractFieldException, UserNotFoundException;

    void unlockUserByLogin(String login) throws AbstractFieldException, UserNotFoundException;
}
