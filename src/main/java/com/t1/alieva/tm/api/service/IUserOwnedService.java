package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.repository.IUserOwnedRepository;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {
    //  M create(String userId, String name) throws AbstractFieldException, AbstractEntityNotFoundException;
    //   M create(String userId, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;
}
