package com.t1.alieva.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);
}
