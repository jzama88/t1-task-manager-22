package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);
}
