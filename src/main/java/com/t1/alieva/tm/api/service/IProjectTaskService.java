package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void removeProjectById(String userId, String projectId) throws AbstractFieldException, AbstractEntityNotFoundException;

    void unbindTaskFromProject(String userId, String projectId, String taskId) throws AbstractFieldException, AbstractEntityNotFoundException;
}
