package com.t1.alieva.tm.api.repository;

import com.t1.alieva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);
}
