package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.entity.UserNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AccessDeniedException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import com.t1.alieva.tm.model.User;

import javax.naming.AuthenticationException;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractUserException, AbstractFieldException, AbstractEntityNotFoundException;

    void login(String login, String password) throws AbstractFieldException, AccessDeniedException, AbstractUserException, UserNotFoundException, AbstractEntityNotFoundException, AuthenticationException, AuthenticException;

    void logout();

    Boolean isAuth();

    String getUserId() throws AbstractUserException;

    User getUser() throws AbstractUserException, AbstractFieldException;

    void checkRoles(Role[] roles) throws AbstractUserException, AbstractFieldException;
}
