package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    Project create(String userId, String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project create(String userId, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateById(String userId, String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusByIndex(String userId, Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Project changeProjectStatusById(String userId, String id, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;
}
