package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);
}
