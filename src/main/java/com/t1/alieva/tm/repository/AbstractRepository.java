package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IRepository;
import com.t1.alieva.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        models.clear();
    }

    @Override
    public M findOneById(final String id) {
        return models
                .stream()
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M removeOne(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public void removeAll(final Collection<M> models) {
        models.removeAll(models);
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }
}
