package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserOwnedRepository;
import com.t1.alieva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {
    @Override
    public void removeAll(String userId) {
      /*  final List<M> models = findAll(userId);
        for (final M model : models) {
            removeOne(model);
        }*/
        removeAll(findAll(userId));
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(String userId) {
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(String userId, String id) {
        if (userId == null || id == null) return null;
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .filter(r -> id.equals(r.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findOneByIndex(String userId, Integer index) {
        return models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    @Override
    public int getSize(String userId) {
        return (int) models
                .stream()
                .filter(r -> userId.equals(r.getUserId()))
                .count();
    }

    @Override
    public M removeOneById(String userId, String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public M removeOneByIndex(String userId, Integer index) {
        if (userId == null || index == null) return null;
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Override
    public M add(String userId, M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);

    }

    @Override
    public M removeOne(String userId, M model) {
        if (userId == null || model == null) return null;
        return removeOneById(userId, model.getId());
    }


}
