package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.util.HashUtil;


public class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @Override
    public User create(String login, String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findOneByLogin(String login) {
        return models
                .stream()
                .filter(r -> login.equals(r.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findOneByEmail(String email) {
        return models
                .stream()
                .filter(r -> email.equals(r.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public boolean isLoginExist(final String login) {
        return models
                .stream()
                .anyMatch(r -> login.equals(r.getLogin()));
    }

    @Override
    public boolean isEmailExist(final String email) {
        return models
                .stream()
                .anyMatch(r -> email.equals(r.getEmail()));
    }
}
